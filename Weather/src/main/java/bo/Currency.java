package bo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

@JsonIgnoreProperties("Date")
public class Currency {
    @JsonProperty("Cur_ID")
    private long id;
    @JsonProperty("Date")
    private LocalDateTime date;
    @JsonProperty("Cur_Abbreviation")
    private String alias;
    @JsonProperty("Cur_Scale")
    private int scale;
    @JsonProperty("Cur_Name")
    private String name;
    @JsonProperty("Cur_OfficialRate")
    private String officialRate;

    public String getofficialRate() {
        return officialRate;
    }
}
