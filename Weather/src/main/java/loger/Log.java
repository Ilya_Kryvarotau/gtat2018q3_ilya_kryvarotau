package loger;

import org.apache.log4j.Logger;

public class Log {

    public static Logger logger = Logger.getLogger("MyLoger");

    public static void trace(String message) {
        logger.trace(message);
    }

    public static void debug(Object message) {
        logger.debug(message);
    }

    public static void info(Object message) {
        logger.info(message);
    }

    public static void warn(Object message) {
        logger.warn(message);
    }

    public static void error(Object message) {
        logger.error(message);
    }

    public static void fatal(Object message) {
        logger.fatal(message);
    }
}
