package runner;

import listener.TestListener;
import loger.Log;
import org.testng.TestNG;

import java.util.Collections;

public class TestRunner {
    public static TestNG createTestNG() {
        TestNG testNG = new TestNG();
        testNG.addListener(new TestListener());
        testNG.setTestSuites(Collections.singletonList("./testng.xml"));
        return testNG;
    }

    public static void main(String[] args) {
        Log.info("Start app");
        createTestNG().run();
        Log.info("Finish app");
    }
}
