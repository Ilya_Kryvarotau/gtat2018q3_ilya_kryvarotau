package tests;

import bo.Currency;
import io.restassured.RestAssured;
import loger.Log;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;

import static java.lang.System.out;

public class CurrencyTest {

    private static final String URL = "http://www.nbrb.by/API/ExRates/Rates/145?onDate=2017-02-23";

    @Test (description = "Verify that the currency rate is displayed on a specific date.")
    public void deserializingTest() {
        Currency[] allCurrencies = RestAssured.get(URL).as(Currency[].class);
        Arrays.stream(allCurrencies).forEach(s -> out.println(s.getofficialRate()));
        Log.info("The official course is displayed in the console");
        Assert.assertTrue(allCurrencies.length > 0, "The number of currencies must be greater than zero.");
    }
}
