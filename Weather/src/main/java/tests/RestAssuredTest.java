package tests;

import io.restassured.RestAssured;
import loger.Log;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;
import java.io.InputStream;

public class RestAssuredTest {
    //private final static String URL_1 = "http://api.worldweatheronline.com/premium/v1/past-weather.ashx"
    // + "?key=cda121efb7024287ba181547190202&q=Homyel&format=xml&date=2017-01-01&enddate=2017-01-31";
    private static final String URL_2 = "http://api.worldweatheronline.com/premium/v1/past-weather.ashx"
                    + "?key=cda121efb7024287ba181547190202&q=Homyel&format=xml&date=2017-02-01&enddate=2017-02-28";
    private static final String XPATH_PATTERN = ".//weather[mintempC>0 and .//hourly[precipMM>0]]";
    Document doc;
    private XPathFactory xpathfactory;
    private XPath xpath;

    @BeforeTest
    public void  buildDom() throws ParserConfigurationException, IOException, SAXException {
        //InputStream response = given().when().get(URL_1).asInputStream();
        InputStream response = RestAssured.given().when().get(URL_2).asInputStream();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        doc = builder.parse(response);
        Log.info("Response is parsed");
        xpathfactory = XPathFactory.newInstance();
        xpath = xpathfactory.newXPath();
    }

    @Test (description = "Verify that the date is displayed when temperature and precipitation greater than zero.")
    public void checkWeather() throws XPathException {
        XPathExpression expr = xpath.compile(XPATH_PATTERN);
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        NodeList nodes = (NodeList) result;
        for (int i = 0; i < nodes.getLength(); i++) {
            System.out.println(nodes.item(i).getFirstChild().getTextContent());
        }
        Log.info("Date is displayed in the console");
        Assert.assertTrue(nodes.getLength() > 0, "The number of dates must be greater than zero.");
    }
}
