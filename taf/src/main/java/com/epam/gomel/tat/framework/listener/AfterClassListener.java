package com.epam.gomel.tat.framework.listener;

import com.epam.gomel.tat.framework.ui.Browser;
import org.testng.IClassListener;
import org.testng.ITestClass;

public class AfterClassListener implements IClassListener {

    @Override
    public void onAfterClass(ITestClass testClass) {
        Browser.getInstance().stopBrowser();
    }
}
