package com.epam.gomel.tat.framework.listener;

import com.epam.gomel.tat.framework.loger.Log;
import com.epam.gomel.tat.framework.ui.Browser;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class TestListener implements IInvokedMethodListener {

    private static final int SUCCESS_STATUS = 1;
    private static final int FAILED_STATUS = 2;
    private static final int SKIPPED_STATUS = 3;

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        Log.info("[METHOD_STARTED] - " + method.getTestMethod().getMethodName());
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        switch (testResult.getStatus()) {
            case SUCCESS_STATUS:
                Log.info("[METHOD_SUCCESS] " + method.getTestMethod().getMethodName());
                break;
            case FAILED_STATUS:
                Log.info("[METHOD_FAILED] " + method.getTestMethod().getMethodName());
                Browser.getInstance().makeScreenshot();
                break;
            case SKIPPED_STATUS:
                Log.info("[METHOD_SKIPPED] " + method.getTestMethod().getMethodName());
                break;
            default:
                throw new RuntimeException("Not supported Status " + testResult.getStatus());
        }
    }
}
