package com.epam.gomel.tat.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        features = "src/main/resources/features",
        glue = "com.epam.gomel.tat.tests.yandex.product.disk.steps",
        tags = "all",
        plugin = {
                "pretty", "json:target/Cucumber.json",
                "html:target/cucumber-html-report"
        }
)
public class CucumberJUnitRunner {

}
