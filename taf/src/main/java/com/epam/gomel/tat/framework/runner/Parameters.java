package com.epam.gomel.tat.framework.runner;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.epam.gomel.tat.framework.ui.BrowserType;

public final class Parameters {

    private static Parameters instance;

    @Parameter(names = "--help", help = false, description = "Help")
    private boolean help;

    @Parameter (names = {"--browser", "-b"}, description = "Browse type",
            converter = BrowserTypeConverter.class, required = true)
    private BrowserType browserType = BrowserType.CHROME;

    @Parameter (names = {"--gecko", "-g"}, description = "Parh to GeckoDriver")
    private String geckoDriver = "target\\geckodriver.exe";

    @Parameter (names = {"--chrome", "-ch"}, description = "Parh to ChromeDriver")
    private String chromeDriver = "target\\chromedriver.exe";

    private Parameters() {
    }

    public static synchronized Parameters instance() {
        if (instance == null) {
            instance = new Parameters();
        }
        return instance;
    }

    public boolean isHelp() {
        return help;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public String getChromeDriver() {
        return chromeDriver;
    }

    public String getGeckoDriver() {
        return geckoDriver;
    }

    public static class BrowserTypeConverter implements IStringConverter<BrowserType> {

        public BrowserType convert(String s) {
            return BrowserType.valueOf(s.toUpperCase());
        }
    }
}
