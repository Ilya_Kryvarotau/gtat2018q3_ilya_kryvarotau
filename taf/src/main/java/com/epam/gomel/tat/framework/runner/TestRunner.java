package com.epam.gomel.tat.framework.runner;

import java.util.Collections;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.epam.gomel.tat.framework.listener.AfterClassListener;
import com.epam.gomel.tat.framework.listener.TestListener;
import com.epam.gomel.tat.framework.loger.Log;
import org.testng.TestNG;

public class TestRunner {

    public static TestNG createTestNG() {
        TestNG testNG = new TestNG();
        testNG.addListener(new TestListener());
        testNG.addListener(new AfterClassListener());
        testNG.setTestSuites(Collections.singletonList("./testng.xml"));
        return testNG;
    }

    public static void main(String[] args) {
        Log.info("Parse cli");
        parseCli(args);
        Log.info("Start app");
        createTestNG().run();
        Log.info("Finish app");
    }

    private static void parseCli(String[] args) {
        Log.info("Parse clis using JComander");
        JCommander jCommander = new JCommander(Parameters.instance());
        try {
            jCommander.parse(args);
        } catch (ParameterException e) {
            Log.error(e.getMessage());
            System.exit(1);
        }
        if (Parameters.instance().isHelp()) {
            jCommander.usage();
            System.exit(0);
        }
    }
}
