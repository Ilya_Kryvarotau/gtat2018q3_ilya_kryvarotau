package com.epam.gomel.tat.framework.ui;

import com.epam.gomel.tat.framework.loger.Log;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public final class Browser implements WrapsDriver {

    static final int WAIT_TEN_SECONDS = 10;
    static final int WAIT_FIVE_SECONDS = 5000;
    private static ThreadLocal<Browser> instance = new ThreadLocal<>();
    private WebDriver wrappedWebDriver;

    public Browser() {
        wrappedWebDriver = BrowserFactory.getBrowser();
    }

    public static synchronized Browser getInstance() {
        if (instance.get() == null) {
            instance.set(new Browser());
        }
        return instance.get();
    }

    public WebDriver getWrappedWebDriver() {
        return wrappedWebDriver;
    }

    public void stopBrowser() {
        try {
            if (getWrappedWebDriver() != null) {
                getWrappedWebDriver().close();
                getWrappedWebDriver().quit();
            }
        } catch (WebDriverException e) {
            Log.error(e.getMessage());
        } finally {
            instance.set(null);
        }
    }

    public void get(String url) {
        Log.info("WebDriver navigate to url: " + url);
        wrappedWebDriver.get(url);
        makeScreenshot();
    }

    public void click(By by) {
        WebElement element = waitForVisibilityOfElement(by);
        highlightBackground(element);
        element.click();
        Log.info("Click " + by);
    }

    public void doubleClick(By by) {
        WebElement element = waitForVisibilityOfElement(by);
        highlightBackground(element);
        Actions builder = new Actions(wrappedWebDriver);
        builder.doubleClick(wrappedWebDriver.findElement(by)).build().perform();
        Log.info("Double click on element " + by);
    }

    public void contextClick(By by) {
        WebElement element = waitForVisibilityOfElement(by);
        highlightBackground(element);
        Actions builder = new Actions(wrappedWebDriver);
        builder.contextClick(wrappedWebDriver.findElement(by)).build().perform();
        Log.info("Click the right mouse button on element " + by);
    }

    public  void  type(By by, String key) {
        WebElement element = waitForVisibilityOfElement(by);
        highlightBackground(element);
        element.sendKeys(key);
        Log.info("Type " + key + " in text field " + by);
    }

    public void clear(By by) {
        wrappedWebDriver
                .findElement(by).sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a"));
    }

    public String getText(By by) {
        waitForVisibilityOfElement(by);
        Log.debug("Get text " + by);
        return wrappedWebDriver.findElement(by).getText();
    }

    public String getUrl() {
        Log.debug("Get URL");
        return wrappedWebDriver.getCurrentUrl();
    }

    public void switchToFrame(By by) {
        if (by == null) {
            wrappedWebDriver.switchTo().defaultContent();
            Log.debug("Switch to default frame ");
        } else {
            wrappedWebDriver.switchTo().frame(wrappedWebDriver.findElement(by));
            Log.debug("Switch to frame " + by);
        }
    }

    public void switchToTab(int tab) {
        ArrayList<String> tabs = new ArrayList<String>(wrappedWebDriver.getWindowHandles());
        wrappedWebDriver.switchTo().window(tabs.get(tab));
        Log.debug("Switch to tab " + tab);
    }

    public void closeTab() {
        try {
            if (wrappedWebDriver != null) {
                wrappedWebDriver.close();
            }
        } catch (WebDriverException e) {
            Log.error(e.getMessage());
        }
    }

    public boolean isVisible(By by) {
        try {
            waitForVisibilityOfElement(by);
        } catch (WebDriverException e) {
            Log.error(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean isVisibleNotWait(By by) {
        try {
            waitForVisibilityOfElement(by, 0);
        } catch (WebDriverException e) {
            Log.error(e.getMessage());
            return false;
        }
        return true;
    }

    public WebElement waitForVisibilityOfElement(By by, long timeout) {
        Log.debug("Wait for visibility of element " + by);
        return new WebDriverWait(wrappedWebDriver, timeout)
        .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public WebElement waitForVisibilityOfElement(By by) {
        return waitForVisibilityOfElement(by, WAIT_TEN_SECONDS);
    }

    public void makeScreenshot() {
        File screenshotFile = new File("logs/screenshots/" + System.nanoTime() + "screenshot.png");
        try {
            File screenshotAsFile = ((TakesScreenshot) wrappedWebDriver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshotAsFile, screenshotFile);
            Log.info("Screenshot taken: file: " + screenshotFile.getAbsolutePath());
        } catch (IOException e) {
            Log.error(e.getMessage());
            throw new RuntimeException("Failed screenshot: ", e);
        }
    }

    public void highlightBackground(WebElement element) {
        String backgroundColor = element.getCssValue("backgroundColor");
        JavascriptExecutor js = (JavascriptExecutor) getWrappedWebDriver();
        js.executeScript("arguments[0].style.backgroundColor = '" + "yellow" + "'", element);
        makeScreenshot();
        js.executeScript("arguments[0].style.backgroundColor = '" + backgroundColor + "'", element);
    }

    public void waitFiveSeconds() {
        try {
            Thread.sleep(WAIT_FIVE_SECONDS);
        } catch (InterruptedException e) {
            Log.error(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public WebDriver getWrappedDriver() {
        return wrappedWebDriver;
    }
}
