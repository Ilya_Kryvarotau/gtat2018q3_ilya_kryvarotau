package com.epam.gomel.tat.framework.ui;

import com.epam.gomel.tat.framework.exception.NotSupportedBrowserException;
import com.epam.gomel.tat.framework.loger.Log;
import com.epam.gomel.tat.framework.runner.Parameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public final class BrowserFactory {

    private BrowserFactory() {

    }

    public static WebDriver getBrowser() {
        WebDriver webDriver;
        switch (Parameters.instance().getBrowserType()) {
            case CHROME:
                System.setProperty("webdriver.chrome.driver", Parameters.instance().getChromeDriver());
                webDriver = new ChromeDriver();
                break;
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver", Parameters.instance().getGeckoDriver());
                webDriver = new FirefoxDriver();
                break;
            default:
                try {
                    throw new NotSupportedBrowserException("Not supported browser "
                            + Parameters.instance().getBrowserType());
                } catch (NotSupportedBrowserException e) {
                    Log.error(e.getMessage());
                    webDriver = null;
                }
        }
        configure(webDriver);
        return webDriver;
    }

    private static void configure(WebDriver webDriver) {
        webDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
    }
}
