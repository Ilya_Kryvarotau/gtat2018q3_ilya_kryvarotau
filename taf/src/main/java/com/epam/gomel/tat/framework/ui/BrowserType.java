package com.epam.gomel.tat.framework.ui;

public enum BrowserType {
    CHROME, FIREFOX
}
