package com.epam.gomel.tat.tests.yandex.product.disk;

import com.epam.gomel.tat.yandex.product.disk.screen.DocumentPage;
import com.epam.gomel.tat.yandex.product.disk.screen.FolderPage;
import com.epam.gomel.tat.yandex.product.disk.screen.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static com.epam.gomel.tat.yandex.product.disk.screen.DocumentPage.*;
import static com.epam.gomel.tat.yandex.product.disk.screen.MenuPage.*;

public class DocumentTest {

    @Test (description =
            "Test verifies that user can create new Word document inside that new folder,"
                    + "past some text inside document and save it.")
    public void createDocumentTest() {
        DocumentPage documentPage = new DocumentPage();
        new LoginPage().loginWithCorrectCredential()
                .openDisk();
        new FolderPage().createFolder();
        documentPage.createDocument();
        documentPage.saveAndClose();
        Assert.assertTrue(documentPage.checkDocumentIsDisplayed(), DOCUMETN_DISPLAYED_MESSAGE);
    }

    @Test (dependsOnMethods = {"createDocumentTest"},
            description = "Test verifies that user can reopen document for editing"
                    + "and see that all information saved correctly.")
    public void changeDocumentTest() {
        DocumentPage documentPage = new DocumentPage();
        documentPage.changeDocument();
        Assert.assertEquals(documentPage.getCurrentTextFromDocument(), "Hello world! ",
                DOCUMETN_CONTAIN_TEXT_MESSAGE);
        documentPage.saveAndClose();
    }

    @Test(dependsOnMethods = {"changeDocumentTest"}, description = "Test verifies that user can move document to trash")
    public void moveDocumentToTrashTest() {
        DocumentPage documentPage = new DocumentPage();
        documentPage.moveToTrash();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(documentPage.getURL(), "https://disk.yandex.ru/client/trash",
                MAIN_MENU_ITEMS_PAGES_MESSAGE);
        softAssert.assertTrue(documentPage.checkDocumentIsDisplayed(), DOCUMETN_DISPLAYED_MESSAGE);
        softAssert.assertAll();
    }
}
