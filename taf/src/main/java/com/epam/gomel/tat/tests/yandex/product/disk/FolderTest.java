package com.epam.gomel.tat.tests.yandex.product.disk;

import com.epam.gomel.tat.yandex.product.disk.screen.FolderPage;
import com.epam.gomel.tat.yandex.product.disk.screen.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.epam.gomel.tat.yandex.product.disk.screen.FolderPage.*;

public class FolderTest {
    @Test (description = "Test verifies that user can create а new folder, with unique name for each new folder,"
            + "and can visit that folder.")
    public void createFolderTest() {
        FolderPage folderPage = new FolderPage();
        new LoginPage().loginWithCorrectCredential()
                .openDisk();
        folderPage.createFolder();
        Assert.assertEquals(folderPage.getCurrentFolderName(), uuid, CREATE_FOLDER_MESSAGE);
    }
}
