package com.epam.gomel.tat.tests.yandex.product.disk;

import com.epam.gomel.tat.yandex.product.disk.screen.LoginPage;
import org.testng.Assert;
import org.testng.annotations.*;

import static com.epam.gomel.tat.yandex.product.disk.screen.LoginPage.*;

public class LoginTest {

    @Test(description = "Test verifies that user get error message, when he login with incorrect credentials")
    public void wrongLoginTest() {
        LoginPage loginPage = new LoginPage();
        loginPage.openPassport();
        if (loginPage.elementIsDisplayed(PASSWORD_INPUT_LOCATOR)) {
            loginPage.wrongLoginWithOneForm();
            Assert.assertTrue(loginPage.elementIsDisplayed(ERROR_MESAGE_2_LOCATOR), DISPLAY_ERROR_MESSAGE);
        } else {
            loginPage.wrongLoginWithTwoForms();
            Assert.assertTrue(loginPage.elementIsDisplayed(ERROR_MESAGE_LOCATOR), DISPLAY_ERROR_MESSAGE);
        }
    }

    @Test (dependsOnMethods = {"wrongLoginTest"},
            description = "Test verifies that user can login with correct credentials")
    public void loginTest() {
        LoginPage loginPage = new LoginPage();
        loginPage.loginWithCorrectCredential();
        Assert.assertEquals(loginPage.getCurrentUserName(), LOGIN, CORRECT_CREDENTIALS_MESSAGE);
    }
}
