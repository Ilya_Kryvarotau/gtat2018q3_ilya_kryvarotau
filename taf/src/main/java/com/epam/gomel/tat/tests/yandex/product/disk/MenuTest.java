package com.epam.gomel.tat.tests.yandex.product.disk;

import com.epam.gomel.tat.yandex.product.disk.screen.LoginPage;
import com.epam.gomel.tat.yandex.product.disk.screen.MenuPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.epam.gomel.tat.yandex.product.disk.screen.MenuPage.*;

public class MenuTest {

    @Test (description = "Test verifies that main menu item Recent work correctly and lead to correct page",
            priority = FIRST_PRIORITY)
    public void checkRecentTest() {
        new LoginPage().loginWithCorrectCredential()
                .openDisk();
        new MenuPage().goToPage(MENU_RECENT_LOCATOR);
        Assert.assertEquals(new MenuPage().getURL(), "https://disk.yandex.ru/client/recent",
                MAIN_MENU_ITEMS_PAGES_MESSAGE);
    }

    @Test (description = "Test verifies that main menu item Disk work correctly and lead to correct page",
            priority = SECOND_PRIORITY)
    public void checkDiskTest() {
        new MenuPage().goToPage(MENU_DISK_LOCATOR);
        Assert.assertEquals(new MenuPage().getURL(), "https://disk.yandex.ru/client/disk",
                MAIN_MENU_ITEMS_PAGES_MESSAGE);
    }

    @Test (description = "Test verifies that main menu item Photo work correctly and lead to correct page",
            priority = THIRD_PRIORITY)
    public void checkPhotoTest() {
        new MenuPage().goToPage(MENU_PHOTO_LOCATOR);
        Assert.assertEquals(new MenuPage().getURL(), "https://disk.yandex.ru/client/photo",
                MAIN_MENU_ITEMS_PAGES_MESSAGE);
    }

    @Test (description = "Test verifies that main menu item Published work correctly and lead to correct page",
            priority = FOURTH_PRIORITY)
    public void checkPublishedTest() {
        new MenuPage().goToPage(MENU_PUBLISHED_LOCATOR);
        Assert.assertEquals(new MenuPage().getURL(), "https://disk.yandex.ru/client/published",
                MAIN_MENU_ITEMS_PAGES_MESSAGE);
    }

    @Test (description = "Test verifies that main menu item Journal work correctly and lead to correct page",
            priority = FIFTH_PRIORITY)
    public void checkJournalTest() {
        new MenuPage().goToPage(MENU_JOURNAL_LOCATOR);
        Assert.assertEquals(new MenuPage().getURL(), "https://disk.yandex.ru/client/journal",
                MAIN_MENU_ITEMS_PAGES_MESSAGE);
    }

    @Test (description = "Test verifies that main menu item Mail work correctly and lead to correct page",
            priority = SIXTH_PRIORITY)
    public void checkMailTest() {
        new MenuPage().goToPage(MENU_MAIL_LOCATOR);
        Assert.assertEquals(new MenuPage().getURL(), "https://disk.yandex.ru/client/mail",
                MAIN_MENU_ITEMS_PAGES_MESSAGE);
    }

    @Test (description = "Test verifies that main menu item Trash work correctly and lead to correct page",
            priority = SEVENTH_PRIORITY)
    public void checkTrashTest() {
        new MenuPage().goToPage(MENU_TRASH_LOCATOR);
        Assert.assertEquals(new MenuPage().getURL(), "https://disk.yandex.ru/client/trash",
                MAIN_MENU_ITEMS_PAGES_MESSAGE);
    }
}
