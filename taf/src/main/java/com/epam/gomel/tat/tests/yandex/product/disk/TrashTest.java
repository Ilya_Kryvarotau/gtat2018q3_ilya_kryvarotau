package com.epam.gomel.tat.tests.yandex.product.disk;

import com.epam.gomel.tat.yandex.product.disk.screen.DocumentPage;
import com.epam.gomel.tat.yandex.product.disk.screen.FolderPage;
import com.epam.gomel.tat.yandex.product.disk.screen.LoginPage;
import com.epam.gomel.tat.yandex.product.disk.screen.TrashPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.epam.gomel.tat.yandex.product.disk.screen.TrashPage.*;

public class TrashTest {
    @Test (description = "Test verifies that user can empty trash and check that document removed completely.")
    public void emptyTrashTest() {
        TrashPage trashPage = new TrashPage();
        new LoginPage().loginWithCorrectCredential().openDisk();
        new FolderPage().createFolder();
        new DocumentPage().createDocument().saveAndClose().moveToTrash();
        trashPage.emptyTrash();
        Assert.assertFalse(trashPage.checkDocumentIsDeleted(), DOCUMETN_NOT_DISPLAYED_MESSAGE);
    }
}
