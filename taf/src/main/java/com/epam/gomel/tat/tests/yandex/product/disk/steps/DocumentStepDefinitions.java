package com.epam.gomel.tat.tests.yandex.product.disk.steps;

import com.epam.gomel.tat.yandex.product.disk.screen.DocumentPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

import static com.epam.gomel.tat.yandex.product.disk.screen.DocumentPage.DOCUMETN_CONTAIN_TEXT_MESSAGE;
import static com.epam.gomel.tat.yandex.product.disk.screen.DocumentPage.DOCUMETN_DISPLAYED_MESSAGE;

public class DocumentStepDefinitions {

    DocumentPage documentPage = new DocumentPage();

    @When("^user creates new document and paste text$")
    public void userCreatesNewDocumentAndPasteText() {
        documentPage.createDocument();
    }

    @And("^user save and close document$")
    public void userSaveAndCloseDocument() {
        documentPage.saveAndClose();
    }

    @Then("^new document created$")
    public void newDocumentCreated() {
        Assert.assertTrue(documentPage.checkDocumentIsDisplayed(), DOCUMETN_DISPLAYED_MESSAGE);
    }

    @And("^user change document$")
    public void userChangeDocument() {
        documentPage.changeDocument();
    }

    @Then("^all information is saved correctly$")
    public void allInformationIsSavedCorrectly() {
        Assert.assertEquals(documentPage.getCurrentTextFromDocument(), "Hello world! ",
                DOCUMETN_CONTAIN_TEXT_MESSAGE);
    }

    @When("^user move document to the trash$")
    public void userMoveDocumentToTheTrash() {
        documentPage.moveToTrash();
    }

    @Then("^document deleted$")
    public void documentDeleted() {
        Assert.assertTrue(documentPage.checkDocumentIsDisplayed(), DOCUMETN_DISPLAYED_MESSAGE);
    }
}
