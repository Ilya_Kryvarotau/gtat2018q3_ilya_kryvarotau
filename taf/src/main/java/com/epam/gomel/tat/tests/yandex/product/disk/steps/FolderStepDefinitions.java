package com.epam.gomel.tat.tests.yandex.product.disk.steps;

import com.epam.gomel.tat.yandex.product.disk.screen.FolderPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.testng.Assert;

import static com.epam.gomel.tat.yandex.product.disk.screen.FolderPage.CREATE_FOLDER_MESSAGE;
import static com.epam.gomel.tat.yandex.product.disk.screen.FolderPage.uuid;

public class FolderStepDefinitions {
    FolderPage folderPage = new FolderPage();

    @And("^user creates new folder$")
    public void userCreatesNewFolder() {
        folderPage.createFolder();
    }

    @Then("^new folder created$")
    public void newFolderCreated() {
        Assert.assertEquals(folderPage.getCurrentFolderName(), uuid, CREATE_FOLDER_MESSAGE);
    }
}
