package com.epam.gomel.tat.tests.yandex.product.disk.steps;

import com.epam.gomel.tat.framework.loger.Log;
import com.epam.gomel.tat.framework.ui.Browser;
import com.epam.gomel.tat.yandex.product.disk.screen.LoginPage;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

import static com.epam.gomel.tat.framework.ui.Browser.getInstance;
import static com.epam.gomel.tat.yandex.product.disk.screen.LoginPage.*;

public class LoginStepDefinitions {

    LoginPage loginPage = new LoginPage();

    @Given("^user is (?:on|on the) home (?:page|screen)$")
    public void userIsOnLoginForm() {
        loginPage.openPassport();
    }

    @When("^user (?:sign in|log in) with valid credentials$")
    public void userHasValidCredentials() {
        loginPage.loginWithCorrectCredential();
    }

    @Then("^user is reaches [Yy]andex disk$")
    public void userIsReachesYandexDisk() {
        Assert.assertEquals(loginPage.getCurrentUserName(), LOGIN, CORRECT_CREDENTIALS_MESSAGE);
    }

    @When("^user sign in with invalid credentials$")
    public void userSignInWithInvalidCredentials() {
        if (loginPage.elementIsDisplayed(PASSWORD_INPUT_LOCATOR)) {
            loginPage.wrongLoginWithOneForm();
        } else {
            loginPage.wrongLoginWithTwoForms();
        }
    }

    @Then("^user gets error message$")
    public void userGetsErrorMessage() {
        if (loginPage.elementIsDisplayed(PASSWORD_INPUT_LOCATOR)) {
            Assert.assertTrue(loginPage.elementIsDisplayed(ERROR_MESAGE_2_LOCATOR), DISPLAY_ERROR_MESSAGE);
        } else {
            Assert.assertTrue(loginPage.elementIsDisplayed(ERROR_MESAGE_LOCATOR), DISPLAY_ERROR_MESSAGE);
        }
    }

    @And("^user open [Yy]andex disk$")
    public void userOpenYandexDisk() {
        loginPage.openDisk();
    }

    @After
    public void tearDown() {
        Log.info("Now browser will be close");
        Browser.getInstance().stopBrowser();
    }

    @And("^user enters (.*) email and (.*) password$")
    public void userEntersInvalidEmailAndInvalidPassword(String email, String password) {
        if (loginPage.elementIsDisplayed(PASSWORD_INPUT_LOCATOR)) {
            loginPage.typeLogin(email);
            loginPage.typePassword(password);
            getInstance().click(PASSWORD_BUTTON_LOCATOR);
        } else {
            loginPage.typeLogin(email);
            getInstance().click(LOGIN_BUTTON_LOCATOR);
            getInstance().waitForVisibilityOfElement(ERROR_MESAGE_LOCATOR);
        }
    }
}
