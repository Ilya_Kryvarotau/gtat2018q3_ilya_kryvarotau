package com.epam.gomel.tat.tests.yandex.product.disk.steps;

import com.epam.gomel.tat.yandex.product.disk.screen.TrashPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

import static com.epam.gomel.tat.yandex.product.disk.screen.TrashPage.DOCUMETN_NOT_DISPLAYED_MESSAGE;

public class TrashStepDefinitions {

    TrashPage trashPage = new TrashPage();

    @When("^user empty the trash$")
    public void userEmptyTheTrash() {
        trashPage.emptyTrash();
    }

    @Then("^trash empty$")
    public void trashEmpty() {
        Assert.assertFalse(trashPage.checkDocumentIsDeleted(), DOCUMETN_NOT_DISPLAYED_MESSAGE);
    }
}
