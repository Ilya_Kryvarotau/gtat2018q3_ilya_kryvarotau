package com.epam.gomel.tat.yandex.product.disk.screen;

import org.openqa.selenium.By;

import static com.epam.gomel.tat.framework.ui.Browser.*;
import static com.epam.gomel.tat.yandex.product.disk.screen.FolderPage.CREATED_BUTTON_LOCATOR;
import static com.epam.gomel.tat.yandex.product.disk.screen.MenuPage.MENU_TRASH_LOCATOR;

public class DocumentPage {

    public static final String DOCUMETN_DISPLAYED_MESSAGE = "Invalid result, the document should be displayed.";
    public static final String DOCUMETN_CONTAIN_TEXT_MESSAGE = "Invalid result, the document must contain text.";
    static final By CREATED_DOCUMENT_LOCATOR =
            By.cssSelector(".create-resource-popup-with-anchor__create-items button:nth-child(2)");
    static final By WORD_DOCUMENT_IFRAME_LOCATOR = By.className("editor-doc__iframe");
    static final By WORD_DOCUMENT_PARAGRAPH_LOCATOR = By.id("WACViewPanel_EditingElement");
    static final By WORD_DOCUMENT_LOCATOR = By.cssSelector(".listing__items > .listing-item_theme_tile");
    static final By EDIT_BUTTON_LOCATOR = By.cssSelector(".menu > div:nth-child(2) > div:nth-child(2)");
    static final By DELETE_BUTTON_LOCATOR = By.cssSelector(".menu > div:nth-child(4) > div:nth-child(2)");

    public DocumentPage createDocument() {
        getInstance().click(CREATED_BUTTON_LOCATOR);
        getInstance().click(CREATED_DOCUMENT_LOCATOR);
        getInstance().switchToTab(1);
        getInstance().waitForVisibilityOfElement(WORD_DOCUMENT_IFRAME_LOCATOR);
        getInstance().switchToFrame(WORD_DOCUMENT_IFRAME_LOCATOR);
        getInstance().waitForVisibilityOfElement(WORD_DOCUMENT_PARAGRAPH_LOCATOR);
        getInstance().type(WORD_DOCUMENT_PARAGRAPH_LOCATOR, "Hello world!");
        getInstance().waitFiveSeconds();
        return this;
    }

    public DocumentPage changeDocument() {
        getInstance().contextClick(WORD_DOCUMENT_LOCATOR);
        getInstance().click(EDIT_BUTTON_LOCATOR);
        getInstance().switchToTab(1);
        getInstance().waitForVisibilityOfElement(WORD_DOCUMENT_IFRAME_LOCATOR);
        getInstance().switchToFrame(WORD_DOCUMENT_IFRAME_LOCATOR);
        getInstance().waitForVisibilityOfElement(WORD_DOCUMENT_PARAGRAPH_LOCATOR);
        return this;
    }

    public DocumentPage saveAndClose() {
        getInstance().closeTab();
        getInstance().switchToTab(0);
        getInstance().waitForVisibilityOfElement(WORD_DOCUMENT_LOCATOR);
        return this;
    }

    public DocumentPage moveToTrash() {
        getInstance().contextClick(WORD_DOCUMENT_LOCATOR);
        getInstance().click(DELETE_BUTTON_LOCATOR);
        getInstance().waitFiveSeconds();
        getInstance().click(MENU_TRASH_LOCATOR);
        getInstance().waitForVisibilityOfElement(WORD_DOCUMENT_LOCATOR);
        return this;
    }

    public boolean checkDocumentIsDisplayed() {
        return getInstance().isVisible(WORD_DOCUMENT_LOCATOR);
    }

    public String getCurrentTextFromDocument() {
        return getInstance().getText(WORD_DOCUMENT_PARAGRAPH_LOCATOR);
    }

    public String getURL() {
        return getInstance().getUrl();
    }
}
