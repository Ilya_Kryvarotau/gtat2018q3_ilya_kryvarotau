package com.epam.gomel.tat.yandex.product.disk.screen;

import org.openqa.selenium.By;
import java.util.UUID;

import static com.epam.gomel.tat.framework.ui.Browser.*;

public class FolderPage {

    public static String uuid;
    public static final String CREATE_FOLDER_MESSAGE =
            "Invalid result, user can create а new folder with unique name, and can visit that folder.";
    static final By CREATED_BUTTON_LOCATOR = By.cssSelector("div.sidebar.sidebar_fixed > div.sidebar__buttons button");
    static final By CREATED_FOLDER_LOCATOR =
            By.cssSelector(".create-resource-popup-with-anchor__create-items button:nth-child(1)");
    static final By CREATE_FOLDER_INPUT_LOCATOR = By.cssSelector(".rename-dialog__rename-form .textinput__control");
    static final By SAVE_FOLDER_BUTTON_LOCATOR = By.cssSelector(".confirmation-dialog__footer button");
    static final By HEADER_LOCATOR = By.tagName("h1");

    public FolderPage createFolder() {
        uuid = UUID.randomUUID().toString();
        getInstance().click(CREATED_BUTTON_LOCATOR);
        getInstance().click(CREATED_FOLDER_LOCATOR);
        getInstance().clear(CREATE_FOLDER_INPUT_LOCATOR);
        getInstance().type(CREATE_FOLDER_INPUT_LOCATOR, uuid);
        getInstance().click(SAVE_FOLDER_BUTTON_LOCATOR);
        getInstance().waitForVisibilityOfElement(By.linkText(uuid));
        getInstance().doubleClick(By.linkText(uuid));
        return this;
    }

    public String getCurrentFolderName() {
        return getInstance().getText(HEADER_LOCATOR);
    }
}
