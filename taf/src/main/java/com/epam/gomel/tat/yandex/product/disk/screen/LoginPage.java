package com.epam.gomel.tat.yandex.product.disk.screen;

import org.openqa.selenium.By;

import static com.epam.gomel.tat.framework.ui.Browser.*;

public class LoginPage {

    public static final String EMAIL = "krivorotovtest@yandex.by";
    public static final String WRONG_EMAIL = "wrong-krivorotovtest@yandex.by";
    public static final String PASSWORD = "krivorotovtest!";
    public static final String WRONG_PASSWORD = "krivorotovtest";
    public static final String LOGIN = "krivorotovtest";
    public static final String START_URL = "https://passport.yandex.ru/";
    public static final String DISK_URL = "https://disk.yandex.ru/";
    public static final String DISPLAY_ERROR_MESSAGE = "Invalid result, user should receive an error message.";
    public static final String CORRECT_CREDENTIALS_MESSAGE =
            "Invalid result, user can login only with correct credentials.";
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By PASSWORD_BUTTON_LOCATOR = By.className("passport-Button");
    public static final By ERROR_MESAGE_2_LOCATOR = By.className("passport-Domik-Form-Error");
    public static final By LOGIN_BUTTON_LOCATOR = By.cssSelector("button.button2");
    public static final By ERROR_MESAGE_LOCATOR = By.className("passp-form-field__error");
    public static final By PASSWORD_INPUT_SECOND_LOCATOR = By.id("passp-field-passwd");
    public static final By PERSONAL_LOGIN_LOCATOR =
            By.cssSelector("div.personal-info-login.personal-info-login__displaylogin");

    public LoginPage openPassport() {
        getInstance().get(START_URL);
        return this;
    }

    public void typeLogin(String login) {
        getInstance().type(LOGIN_INPUT_LOCATOR, login);
    }

    public void typePassword(String login) {
        getInstance().type(PASSWORD_INPUT_LOCATOR, login);
    }

    public LoginPage wrongLoginWithOneForm() {
        getInstance().type(LOGIN_INPUT_LOCATOR, WRONG_EMAIL);
        getInstance().type(PASSWORD_INPUT_LOCATOR, PASSWORD);
        getInstance().click(PASSWORD_BUTTON_LOCATOR);
        getInstance().waitForVisibilityOfElement(ERROR_MESAGE_2_LOCATOR);
        return this;
    }

    public LoginPage wrongLoginWithTwoForms() {
        getInstance().type(LOGIN_INPUT_LOCATOR, WRONG_EMAIL);
        getInstance().click(LOGIN_BUTTON_LOCATOR);
        getInstance().waitForVisibilityOfElement(ERROR_MESAGE_LOCATOR);
        return this;
    }

    public LoginPage loginWithOneForm() {
        getInstance().type(LOGIN_INPUT_LOCATOR, EMAIL);
        getInstance().type(PASSWORD_INPUT_LOCATOR, PASSWORD);
        getInstance().click(PASSWORD_BUTTON_LOCATOR);
        getInstance().waitForVisibilityOfElement(PERSONAL_LOGIN_LOCATOR);
        return this;
    }

    public LoginPage loginWithTwoForms() {
        getInstance().type(LOGIN_INPUT_LOCATOR, EMAIL);
        getInstance().click(LOGIN_BUTTON_LOCATOR);
        getInstance().waitForVisibilityOfElement(PASSWORD_INPUT_SECOND_LOCATOR);
        getInstance().type(PASSWORD_INPUT_SECOND_LOCATOR, PASSWORD);
        getInstance().click(LOGIN_BUTTON_LOCATOR);
        getInstance().waitForVisibilityOfElement(PERSONAL_LOGIN_LOCATOR);
        return this;
    }

    public LoginPage loginWithCorrectCredential() {
        openPassport();
        if (getInstance().isVisibleNotWait(PASSWORD_INPUT_LOCATOR)) {
            loginWithOneForm();
        } else {
            loginWithTwoForms();
        }
        return this;
    }

    public String getCurrentUserName() {
        return getInstance().getText(PERSONAL_LOGIN_LOCATOR);
    }

    public boolean elementIsDisplayed(By arg) {
        return getInstance().isVisibleNotWait(arg);
    }

    public LoginPage openDisk() {
        getInstance().get(DISK_URL);
        return this;
    }
}
