package com.epam.gomel.tat.yandex.product.disk.screen;

import org.openqa.selenium.By;

import static com.epam.gomel.tat.framework.ui.Browser.*;

public class MenuPage {

    public static final String MAIN_MENU_ITEMS_PAGES_MESSAGE =
            "Invalid result, main menu items should lead to correct page.";
    public static final By MENU_RECENT_LOCATOR = By.xpath("//div[@class='root']//a[ @href='/client/recent']");
    public static final By MENU_DISK_LOCATOR = By.xpath("//div[@class='root']//a[ @href='/client/disk']");
    public static final By MENU_PHOTO_LOCATOR = By.xpath("//div[@class='root']//a[ @href='/client/photo']");
    public static final By MENU_PUBLISHED_LOCATOR = By.xpath("//a[@href='/client/shared']");
    public static final By MENU_JOURNAL_LOCATOR = By.xpath("//div[@class='root']//a[ @href='/client/journal']");
    public static final By MENU_MAIL_LOCATOR = By.xpath("//a[@href='/client/mail']");
    public static final By MENU_TRASH_LOCATOR = By.id("/trash");
    public static final int FIRST_PRIORITY = 1;
    public static final int SECOND_PRIORITY = 2;
    public static final int THIRD_PRIORITY = 3;
    public static final int FOURTH_PRIORITY = 4;
    public static final int FIFTH_PRIORITY = 5;
    public static final int SIXTH_PRIORITY = 6;
    public static final int SEVENTH_PRIORITY = 7;

    public MenuPage goToPage(By locator) {
        getInstance().click(locator);
        return this;
    }

    public String getURL() {
        return getInstance().getUrl();
    }
}
