package com.epam.gomel.tat.yandex.product.disk.screen;

import com.epam.gomel.tat.framework.loger.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import static com.epam.gomel.tat.framework.ui.Browser.*;
import static com.epam.gomel.tat.yandex.product.disk.screen.DocumentPage.WORD_DOCUMENT_LOCATOR;

public class TrashPage {

    public static final String DOCUMETN_NOT_DISPLAYED_MESSAGE = "Invalid result, the document shouldn't be displayed.";
    public static final By TRASH_BUTTON_LOCATOR = By.className("client-listing__clean-trash-button");
    public static final By CLEAN_TRASH_BUTTON_LOCATOR = By.className("_nb-small-action-button");

    public TrashPage emptyTrash() {
        getInstance().waitForVisibilityOfElement(TRASH_BUTTON_LOCATOR);
        getInstance().click(TRASH_BUTTON_LOCATOR);
        getInstance().waitForVisibilityOfElement(CLEAN_TRASH_BUTTON_LOCATOR);
        getInstance().click(CLEAN_TRASH_BUTTON_LOCATOR);
        getInstance().waitFiveSeconds();
        return this;
    }

    public boolean checkDocumentIsDeleted() {
        try {
            return getInstance().isVisibleNotWait(WORD_DOCUMENT_LOCATOR);
        } catch (NoSuchElementException e) {
            Log.error(e.getMessage());
            return false;
        }
    }
}
