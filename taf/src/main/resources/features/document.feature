Feature: Create documen
  As a yandex disk user
  I want to create new Word document inside new folder,
  I want to past some text inside the document and save it,
  I want to reopen the document for editing and see that all information is saved correctly,
  I want to move the document to the trash

  Background:
    Given user is on home page
    And user log in with valid credentials
    And user open Yandex disk
    And user creates new folder

  @all
  Scenario: Successful document creation inside new folder
    When user creates new document and paste text
    And user save and close document
    Then new document created

  @all
  Scenario: Successful opening of the document for editing and sees that all information is saved correctly
    When user creates new document and paste text
    And user save and close document
    And user change document
    Then all information is saved correctly

  @all
  Scenario: Successful moving the document to the trash
    And user creates new document and paste text
    And user save and close document
    When user move document to the trash
    Then document deleted