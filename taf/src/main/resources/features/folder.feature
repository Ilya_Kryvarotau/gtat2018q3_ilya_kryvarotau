Feature: Create folder
  As a yandex disk user
  I want to create new folder with unique name and can visit that folder

  @all
  Scenario: Successful login with valid credentials
    Given user is on home page
    When user sign in with valid credentials
    And user open Yandex disk
    And user creates new folder
    Then new folder created