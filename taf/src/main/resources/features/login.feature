Feature: Yandex login
  As a yandex disk user
  I want to reach yandex disk after successful login

  Background:
  Given user is on home page

  @all
  Scenario: Successful login with valid credentials
    When user sign in with valid credentials
    Then user is reaches yandex disk

  @all
  @negative
  Scenario Outline: Unsuccessful login with invalid credentials
    When user enters <email> email and <password> password
    Then user gets error message

    Examples:
      | email                          | password        |
      | wrong-krivorotovtest@yandex.by | krivorotovtest  |
      | wrong-krivorotovtest@yandex.by | krivorotovtest! |
      | krivorotovtest@yandex.by       | krivorotovtest  |