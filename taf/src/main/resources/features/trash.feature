Feature: Empty trash
  As a yandex disk user
  I want to empty the trash and check that document removed completely

  @all
  Scenario: Successful empty the trash
    Given user is on home page
    And user log in with valid credentials
    And user open Yandex disk
    And user creates new folder
    And user creates new document and paste text
    And user save and close document
    And user move document to the trash
    When user empty the trash
    Then trash empty