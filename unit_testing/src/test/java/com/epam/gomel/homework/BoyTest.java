package com.epam.gomel.homework;

import org.testng.annotations.*;
import java.time.Month;
import static org.testng.Assert.*;

public class BoyTest {

    private Girl girlPretty;
    private Girl noPrettyGirl;
    private Boy boyExcellent;
    private Boy boyGood;
    private Boy boyNeutral;
    private Boy boyBadRich;
    private Boy boyBadPoor;
    private Boy boyBadGirlfriendPretty;
    private Boy boyBadSummerMonth;
    private Boy boyHorrible;
    private Boy boyWithoutPrettyGirl;

    @BeforeClass(groups = {"boy"})
    public void beforeMethod()
    {
        girlPretty = new Girl(true);
        noPrettyGirl = new Girl(false);
        boyExcellent = new Boy(Month.JUNE, Human.RICH, girlPretty);
        boyGood = new Boy(Month.JANUARY, Human.RICH, girlPretty);
        boyNeutral = new Boy(Month.JUNE, Human.RICH);
        boyBadRich = new Boy(Month.JANUARY, Human.RICH);
        boyBadPoor = new Boy(Month.JANUARY, Human.POOR);
        boyBadGirlfriendPretty = new Boy(Month.JANUARY, Human.POOR, girlPretty);
        boyBadSummerMonth = new Boy(Month.JUNE);
        boyHorrible = new Boy(Month.JANUARY);
        boyWithoutPrettyGirl = new Boy(Month.JANUARY, Human.RICH, noPrettyGirl);
        
    }

    @Test(groups = {"boy"},priority = 1)
    public void testGetMood() {
        Mood excellent = boyExcellent.getMood();
        assertEquals(Mood.EXCELLENT, excellent);

        Mood good = boyGood.getMood();
        assertEquals(Mood.GOOD, good);

        Mood neutral = boyNeutral.getMood();
        assertEquals(Mood.NEUTRAL, neutral);

        //isRich() == true
        Mood badRich = boyBadRich.getMood();
        boolean a = boyBadRich.isRich();
        boolean b = boyBadRich.isPrettyGirlFriend();
        boolean c = boyBadRich.isSummerMonth();
        assertTrue(a);
        assertFalse(b);
        assertFalse(c);
        assertEquals(Mood.BAD, badRich);

        //isPrettyGirlFriend() == true
        Mood badGirlfriendPretty = boyBadGirlfriendPretty.getMood();
        a = boyBadGirlfriendPretty.isRich();
        b = boyBadGirlfriendPretty.isPrettyGirlFriend();
        c = boyBadGirlfriendPretty.isSummerMonth();
        assertFalse(a);
        assertTrue(b);
        assertFalse(c);
        assertEquals(Mood.BAD, badGirlfriendPretty);

        //isSummerMonth() == true
        Mood badSummerMonth = boyBadSummerMonth.getMood();
        a = boyBadSummerMonth.isRich();
        b = boyBadSummerMonth.isPrettyGirlFriend();
        c = boyBadSummerMonth.isSummerMonth();
        assertFalse(a);
        assertFalse(b);
        assertTrue(c);
        assertEquals(Mood.BAD, badSummerMonth);

        Mood horrible = boyHorrible.getMood();
        assertEquals(Mood.HORRIBLE, horrible);

    }

    @Test(groups = {"boy"},priority = 5)
    public void testSpendSomeMoney() {
        double smallAmountForSpending = Human.POOR;
        double largeAmountForSpending = Human.RICH;

        boyBadRich.spendSomeMoney(smallAmountForSpending);
        assertEquals(boyBadRich.getWealth(), Human.RICH - smallAmountForSpending, 0.001);

        boolean secondAssertPassed = false;
        try
        {
            boyBadPoor.spendSomeMoney(largeAmountForSpending);
        }
        catch (RuntimeException e)
        {
            final String msg = String.format("Not enough money! Requested amount is %s$ but you can't spend more then %s$",
                    largeAmountForSpending, boyBadPoor.getWealth());
            assertEquals(msg, e.getMessage());
            secondAssertPassed = true;
        }
        finally
        {
            assertTrue(secondAssertPassed);
        }
    }

    @Test(groups = {"boy"},priority = 4)
    public void testIsSummerMonth() {
        for (int i = 0; i <= 11; i++)
        {
            Boy boy = new Boy(Month.values()[i]);
            if (i > 4 & i <8)
            {
                assertEquals(true, boy.isSummerMonth());
            }
            else
            {
                assertEquals(false, boy.isSummerMonth());
            }
        }
    }

    @Test(groups = {"boy"},priority = 3)
    public void testIsRich() {
        assertEquals(boyBadRich.isRich(), true);
        assertEquals(boyBadPoor.isRich(), false);
    }

    @Test(groups = {"boy"},priority = 2)
    public void testIsPrettyGirlFriend() {
        //getGirlFriend() == null
        assertFalse(boyHorrible.isPrettyGirlFriend());
        //getGirlFriend().isPretty() == false
        assertFalse(boyWithoutPrettyGirl.isPrettyGirlFriend());
        //getGirlFriend() != null && getGirlFriend().isPretty();
        assertTrue(boyGood.isPrettyGirlFriend());
    }
}