package com.epam.gomel.homework;

import org.testng.annotations.*;
import java.time.Month;
import static org.testng.Assert.*;

public class GirlTest {

    private Boy boyRich;
    private Boy boyPoor;
    private Girl girlExcellent;
    private Girl girlPrettyBoyfriendPoor;
    private Girl girlUglyBoyfriendRich;
    private Girl girlNeutral;
    private Girl girlHate;
    private Girl girlPretty;
    private Girl girlUglyBoyfriendPoor;

    private int param;
    public GirlTest(int param)
    {
        this.param = param;
    }

    @BeforeClass(groups = {"girl"})
    public void beforeMethod()
    {
        boyRich = new Boy(Month.JUNE, Human.RICH);
        boyPoor = new Boy(Month.JUNE, Human.POOR);
        girlExcellent = new Girl(true, true, boyRich);
        girlPrettyBoyfriendPoor = new Girl(true, false, boyPoor);
        girlUglyBoyfriendRich = new Girl(false, false, boyRich);
        girlUglyBoyfriendPoor = new Girl(false, false, boyPoor);
        girlNeutral = new Girl();
        girlHate = new Girl(false, false);
        girlPretty = new Girl(true, true);
    }

    @Test(groups = {"girl"})
    public void testGetMood() {

        Mood excellent = girlExcellent.getMood();
        GirlMatcher.assertThat(Mood.EXCELLENT, GirlMatcher.matches(excellent));

        //isPretty() == true
        Mood goodBeautiful = girlPrettyBoyfriendPoor.getMood();
        boolean a = girlPrettyBoyfriendPoor.isPretty();
        boolean b = girlPrettyBoyfriendPoor.isBoyfriendRich();
        assertTrue(a);
        assertFalse(b);
        assertEquals(Mood.GOOD, goodBeautiful);

        ///isBoyfriendRich() == true
        Mood goodRich = girlUglyBoyfriendRich.getMood();
        a = girlUglyBoyfriendRich.isPretty();
        b = girlUglyBoyfriendRich.isBoyfriendRich();
        assertFalse(a);
        assertTrue(b);
        assertEquals(Mood.GOOD, goodRich);

        Mood neutral = girlNeutral.getMood();
        GirlMatcher.assertThat(Mood.NEUTRAL, GirlMatcher.matches(neutral));

        Mood hate = girlHate.getMood();
        assertEquals(Mood.I_HATE_THEM_ALL, hate);
    }

    @Test(groups = {"girl"})
    public void testSpendBoyFriendMoney() {
        //Boyfriend is rich
        girlExcellent.spendBoyFriendMoney(Human.RICH);
        GirlMatcher.assertThat(boyRich.getWealth(), GirlMatcher.matches(0));
        //Boyfriend is poor
        girlPrettyBoyfriendPoor.spendBoyFriendMoney(Human.POOR);
        GirlMatcher.assertThat(boyPoor.getWealth(), GirlMatcher.matches(Human.POOR));
    }

    @Test(groups = {"girl"})
    public void testIsBoyfriendRich() {

        //getBoyFriend() == null
        assertFalse(girlPretty.isBoyfriendRich());
        //getBoyFriend().isRich() == false
        assertFalse(girlPrettyBoyfriendPoor.isBoyfriendRich());
        //getBoyFriend() != null && getBoyFriend().isRich()
        assertTrue(girlExcellent.isBoyfriendRich());
    }

    @Test(groups = {"girl"})
    public void testIsBoyFriendWillBuyNewShoes() {
        //isPretty() == false
        //isBoyfriendRich() == false
        assertFalse(girlUglyBoyfriendPoor.isBoyFriendWillBuyNewShoes());
        //isPretty() == false
        //isBoyfriendRich() == true
        assertFalse(girlUglyBoyfriendRich.isBoyFriendWillBuyNewShoes());
        //isPretty() == true
        //isBoyfriendRich() == false
        assertFalse(girlPrettyBoyfriendPoor.isBoyFriendWillBuyNewShoes());
        //isPretty() == true
        //isBoyfriendRich() == true
        assertTrue(girlExcellent.isBoyFriendWillBuyNewShoes());
    }

    @Test (groups = {"girl"},dataProvider = "girlData")
    public void testIsSlimFriendBecameFat(Girl dataProvider) {
        //isPretty() == false
        //isSlimFriendGotAFewKilos() == true
        assertTrue(girlNeutral.isSlimFriendBecameFat());
        //isPretty() == false
        //isSlimFriendGotAFewKilos() == false
        assertFalse(dataProvider.isSlimFriendBecameFat());
    }

    @DataProvider(name = "girlData")
    public Object[][] getGirlData()
    {
        return new Object[][]
                {       {girlHate},
                        {girlPretty},
                        {girlPrettyBoyfriendPoor}
                };
    }

    @Test
    @Parameters("testName")
    public void parameterAndFactory(String testName)
    {
        System.out.println("Parameterized value is : " + testName); //parameterization
        System.out.println("Factory index is : " + param); //factory
    }
}