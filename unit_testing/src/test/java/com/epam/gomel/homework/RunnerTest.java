package com.epam.gomel.homework;

import java.util.Arrays;
import java.util.List;

import org.testng.TestNG;

public class RunnerTest
{

    public static void main(String[] args)
    {
        TestNG testNG = new TestNG();
        testNG.addListener(new ListenerTest());
        List<String> files = Arrays.asList("./src/test/resources/suite/test.xml");
        testNG.setTestSuites(files);
        testNG.run();
    }
}