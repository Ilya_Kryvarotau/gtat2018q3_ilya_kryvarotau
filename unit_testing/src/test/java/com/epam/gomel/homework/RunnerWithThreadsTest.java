package com.epam.gomel.homework;

import java.util.Arrays;
import java.util.List;

import org.testng.TestNG;

public class RunnerWithThreadsTest
{

    public static void main(String[] args)
    {
        TestNG testNG = new TestNG();
        testNG.addListener(new ListenerTest());
        List<String> files = Arrays.asList("./src/test/resources/suite/testThreads.xml");
        testNG.setTestSuites(files);
        testNG.run();
    }
}